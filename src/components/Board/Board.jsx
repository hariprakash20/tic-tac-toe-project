import React from "react";
import Square from "../Square/Square";

export default function Board({ nextPlayer, squares, onPlay }) {
  const winner = calculateWinner(squares);

  let status = winner ? "Winner: " + winner : "Next player: " + nextPlayer;

  function handleClick(i) {
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    const nextSquares = [...squares];
    nextSquares[i] = nextPlayer==='X'? 'X':'O';
    onPlay(nextSquares);
  }

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return squares[a];
      }
    }
    return null;
  }
  let board = [];
  {
    for (let i = 0; i < 3; i++) {
      board.push(<div key={"row"+i} className="board-row"></div>);
      for (let j = 0; j < 3; j++) {
        board.push(<Square value={squares[i*3+j]} onSquareClick={() => handleClick(i*3+j)} key={"square"+(i*3+j)}/>);
      }
    }
  }

  return (
    <>
      <div className="board">
        <div className="status">{status}</div>
        {board}
      </div>
    </>
  );
}


