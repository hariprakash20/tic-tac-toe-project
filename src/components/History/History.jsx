import React from "react";

export default function History({moves, toggleOrder}) {
  return (
    <div key={"game-info"} className="game-info">
      <button
        key={"toggle-order"}
        className="toggle-order"
        onClick={toggleOrder}
      >
        toggle Order
      </button>
      <ol key={"all-moves"}>{moves}</ol>
    </div>
  );
}
