import React from "react";
import { useState } from "react";
import Board from "../Board/Board";
import History from "../History/History";

export default function Game() {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const currentSquares = history[currentMove];
  const nextPlayer = currentMove % 2 === 0 ? "X" : "O";

  function handlePlay(nextSquares, nextPlayer) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);
  }

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
  }

  const moves = history.map((squares, move) => {
    if (move !== currentMove) {
      let description = move > 0 ? "Go to move #" + move : "Go to game start";
      return (
          <li key={"moveid:" + move}>
            <button key={"buttonid:" + move} onClick={() => jumpTo(move)}>
              {description}
            </button>
          </li>
      );
    } else {
      return (
        <li key={"moveid:" + move}>
          <p key={"pid:" + move}>You are at move #{move}</p>
        </li>
      );
    }
  });

  function toggleOrder() {
    // console.log(history.map((each) => console.log(each)));
  }

  return (
    <div key={"game"} className="game">
      <div key={"game-board"} className="game-board">
        <Board
          nextPlayer={nextPlayer}
          squares={currentSquares}
          onPlay={handlePlay}
          key={"board"}
        />
      </div>
      <History moves={moves} onClick={toggleOrder} />
    </div>
  );
}
