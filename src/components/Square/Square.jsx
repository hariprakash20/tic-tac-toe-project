import React from "react";

export default function Square({ value, onSquareClick }) {
  return (
    <button className="square" onClick={onSquareClick}>
      {value && value}&nbsp;
    </button>
  );
}
